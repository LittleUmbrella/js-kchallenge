'use strict';
class ElevatorController{
    
    constructor (fromFloor, toFloor) {
        var self = this;
        
        self._elevators = []];
        self._to = toFloor;
    }

    addElevator(elevator){
        let self = this;

        self._elevators.push(elevator);
    }

    //for maintenance mostly (but maybe faults)
    removeElevator(elevator){
        let self = this;

        self._elevators.slice(self._elevators.indexOf(elevator), 1);
    }

    addRequest(request){
        // When an elevator request is made, the unoccupied elevator closest to it will answer the
        // call, unless an occupied elevator is moving and will pass that floor on its way. The
        // exception is that if an unoccupied elevator is already stopped at that floor, then it will
        // always have the highest priority answering that call.

        //implement by looping the elevators.
        //set up the first elevator to be the candidate and then replace as better ones are found in loop
    }
}


module.exports = FloorRequest;