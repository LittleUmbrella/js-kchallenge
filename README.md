run with node:

```
$ node ElevatorSimulator
```

* Thinking about elevator state patterns for door open/close, moving up, moving down.  
    * Doesn't seem to fit obviously, so refactor to pattern if code merits it
* Thinking about chain of responsibility pattern for elevators handling requests themselves, instead of a controller assigning
    * chain of responsibility pattern doesn't seem like a natural fit, as that is more for when each unit (elevator) can decide on its own if it can process the action.  In this case each would have to examine all the rest.  Might need more thought.

* Initial thought is that Controller would be N complexity and Elevators being responsible would be N<sup>2</sup>, unless each elevator not only decides weather to act for themselves, but aslo wheather assign to another (seems odd).

* Let's try controller deciding which elevator to add the requests to and let the elevators handle their own requests (that part seems more or less obvious, but one could imagine a global queue that the controller manages, too.  That's an awful thought.)

* Ran out of time!  Now I've got to go look online to see if I'm dumb (wait, no I don't!)