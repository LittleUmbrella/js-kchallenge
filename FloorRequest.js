'use strict';
class FloorRequest{
    
    constructor (fromFloor, toFloor) {
        var self = this;
        
        self._from = fromFloor;
        self._to = toFloor;
    }

    get fromFloor () { return this._from;}
    get toFloor () { return this._to; }
}


module.exports = FloorRequest;