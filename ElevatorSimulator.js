'use strict';
const Elevator = require("./Elevator");
const FloorRequest = require("./FloorRequest");

//move as much as possible to controller.

var self = this, numberOfElevators = 1, numberOfFloors = 10;

self._elevators  = [];

var f = numberOfFloors, e = numberOfElevators;

while (e > 0 ){
    self._elevators.push(new Elevator(numberOfFloors, null));
    e--;
}

//for now, just move all 
self._elevators.forEach(function(elevator){
    elevator.addRequest(new FloorRequest(5, 3));
    elevator.addRequest(new FloorRequest(1, 10));
    elevator.start();
})
    