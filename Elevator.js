'use strict';

class Elevator{
    
    constructor (numberOfFloors, controller) {
        let self = this;

        self._floorCnt = numberOfFloors;
        self._requests = [];
        self._currentFloorNumber = 1;
        self._occupied = false;
        self._tripCnt = 0;
        self._tripCntMax = 500;

        self._controller = controller; 
        // self.setState = 
    }

    get currentFloorNumber(){return this._currentFloorNumber;}

    addRequest(request){
        let self = this;
        
        //error check here to meet requirement
        if (request.toFloor > self._floorCnt || request.toFloor < 1) throw Error('This elevator does not go to heaven or hell (depending on music played)');
        if (request.fromFloor > self._floorCnt || request.fromFloor < 1) throw Error('Terrestrials only');

        //split requests into onboarding/offboarding
        //todo
        
        console.log("Request added: From floor " + request.fromFloor + " to floor " + request.toFloor);

        //naive implementation.  should priority order the requests
        self._requests.push(request);
    }

    start(){
        let self = this;

        //simulate time to move
        setInterval(function(){
            self.proceed();    
        }, 500);
    }

    proceed(){
        let self = this;

        if (self._requests.length == 0) return; //or go to first floor?

        let requestFloorNumber = self._requests[self._requests.length - 1].toFloor;

        if (self._currentFloorNumber == requestFloorNumber){   
            self._processRequests();   
        }
        else if (self._currentFloorNumber > requestFloorNumber){
            self.moveDown();
        }
        else{
            self.moveUp();
        }

    }

    moveUp(){
        let self = this;
        
        self._currentFloorNumber++;   
        console.log("I'm on floor " + self._currentFloorNumber);
    }

    moveDown(){
        let self = this;

        self._currentFloorNumber--;
        console.log("I'm on floor " + self._currentFloorNumber);
    }

    _processRequests(){
        let self = this;

        if (self._currentFloorNumber == self._requests[self._requests.length - 1].toFloor){
            console.log('my door is open');
            self._requests.pop();
            console.log('my door is closed');
            self._tripCnt++;

            self._maintainIfNecessary();
        }
    }

    _maintainIfNecessary(){
        let self = this;

        if (self._tripCnt == self._tripCntMax){
            self._maintain();
        }
    }

    _maintain(){
        let self = this;

        self._controller.removeElevator(self);
    }
}

module.exports = Elevator;